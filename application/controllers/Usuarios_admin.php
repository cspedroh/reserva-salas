<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_admin extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->controller = 'usuarios_admin';
		$this->load->model(
					array(
						'usuarios_model'
					)
				);

		if (!$this->ion_auth->is_admin()) {
			redirect('admin');
		}
		$this->user = $this->ion_auth->user()->row();
	}

	public function index()
	{
		$usuarios = $this->ion_auth->users()->result();

		$usuarios = $this->get_users_groups($usuarios);

		$dados = array(
				"title" => "Usuários",
				"pasta" => "usuarios",
				"tela" => "retrieve",
				"usuarios" => $usuarios
			);

		$this->load->view('admin/layout_admin', $dados);
	}

	public function create()
	{
		$grupo = 2; 
		
		if($this->input->post()){
			$dados = $this->input->post();

			$username = $dados['nome'];
			$password = $dados['senha'];
			$email = $dados['email'];
			$additional_data = array(
									'first_name' => $dados['nome'],
									'last_name' => '',
								);

			$group = array($grupo); 

			if($this->ion_auth->register($username, $password, $email, $additional_data, $group)){ // verifica se email não existe
				
				redirect('/usuarios_admin/', 'refresh');

			} else{
				$this->session->set_flashdata('erro','Esse e-mail já está cadastrado.');

				$tipoUser = "";

				$tipoUser = 'Usuários';

				$dados = array(
					"action" => base_url().$this->controller.'/create/',
					"title" => "Criar ".$tipoUser,
					"pasta" => "usuarios",
					"tela" => "create"
				);

				$this->load->view('admin/layout_admin', $dados);
			}
			
		}else{
			$tipoUser = 'Usuários';

			$dados = array(
					"action" => base_url().$this->controller.'/create/',
					"title" => "Criar ".$tipoUser,
					"pasta" => "usuarios",
					"tela" => "create"
				);

			$this->load->view('admin/layout_admin', $dados);
		}
	}

	public function update($idUser = '')
	{
		if($idUser != ''){
			
			if($this->input->post()){
				$dados = $this->input->post();

				$data = array(
							'first_name' => $dados['nome'],
							'last_name' => ''
						);

				($dados['senha'] != "") ? $data['password'] = $dados['senha'] : "";
	
				if($this->ion_auth->update($idUser, $data)){ // verifica se email não existe
					
					redirect('/usuarios_admin/', 'refresh');
	
				} else{
					$user = $this->ion_auth->user($idUser)->row();

					$this->session->set_flashdata('erro','Não foi pssível atualizar.');
	
					$tipoUser = "";
	
					$tipoUser = 'Usuários';
	
					$dados = array(
						"action" => base_url().$this->controller.'/update/'.$idUser,
						"title" => "Atualizar ".$tipoUser,
						"pasta" => "usuarios",
						"tela" => "create",
						"user" => $user,
						"update" => 1
					);
	
					$this->load->view('admin/layout_admin', $dados);
				}
			}else{
				$user = $this->ion_auth->user($idUser)->row();
	
				$tipoUser = 'Usuários';
	
				$dados = array(
						"action" => base_url().$this->controller.'/update/'.$idUser,
						"title" => "Atualizar ".$tipoUser,
						"pasta" => "usuarios",
						"tela" => "create",
						"user" => $user,
						"update" => 1
					);
	
				$this->load->view('admin/layout_admin', $dados);
			}
		}
	}

	public function delete($idUser = '')
	{
		if ($idUser != '') {
			
			$this->ion_auth->delete_user($idUser);
			$this->usuarios_model->deleteReservas($idUser);

			redirect('/usuarios_admin/', 'refresh');
		}
	}

	// Retorna array de clientes e com os grupos que fazer parte, exceto de usuário for admin
	private function get_users_groups($users = '')
	{
		if ($users != "") {
			// pre($users);
			foreach ($users as $key => $value) {
				$groups = $this->ion_auth->get_users_groups($users[$key]->id)->result();

				if ($groups != "") {
					$i = 0;
					foreach ($groups as $group) {
						if ($group->id == 1) {
							unset($users[$key]);
							$i = 1;
							break;
						}
					}
					if ($i == 0) {
						$users[$key]->groups = $groups;
					}
				}
				else{
					$users[$key]->groups = $groups;
				}
			}
			return $users;
		}
	}

}