<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salas extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->controller = 'salas';
		$this->load->model(
					array(
						'salas_model'
					)
				);

		if (!$this->ion_auth->logged_in()) {
			redirect('home');
		}

		if ($this->ion_auth->is_admin()) {
			redirect('home');
		}
		else{
			$this->user = $this->ion_auth->user()->row();
			$this->username = $this->user->first_name;
			$this->userId = $this->user->id;
		}

		date_default_timezone_set('America/Sao_Paulo');
	}

	public function index()
	{
		$salas = $this->salas_model->getSalas();

		$dados = array(
			"title" => "Salas",
			"pasta" => "salas",
			"tela" => "retrieve",
			"salas" => $salas,
			"username" => $this->username
		);

		$this->load->view('sistema/layout', $dados);
	}
	
	public function reservar($idSala = '')
	{
		if ($idSala != '') {
			if ($this->input->post()) {
				$dados = $this->input->post();

				$hoje = date('Y-m-d');
				$horario = date('H:i');

				$dados['data_fim'] = date('Y-m-d', strtotime('+60 minute', strtotime($dados['data_inicio']." ".$dados['hora_inicio'])));

				$dados['hora_fim'] = date('H:i', strtotime('+60 minute', strtotime($dados['data_inicio']." ".$dados['hora_inicio'])));

				$horarios = array(
					'inicio_reserva' => $dados['data_inicio']." ".$dados['hora_inicio'],
					'fim_reserva' => $dados['data_fim']." ".$dados['hora_fim']
				);

				if ($hoje < $dados['data_inicio']) {

					if(!$this->salas_model->verificarDisponibilidadeSala($idSala, $horarios)){
						if(!$this->salas_model->verificarDisponibilidadeUsuario($this->userId, $horarios)){
							$this->salas_model->realizarReserva($this->userId, $idSala, $horarios);
							$this->session->set_flashdata('cadastro_ok', "1");
							redirect($this->controller."/reservar/".$idSala);
						}
						else {
							$this->session->set_flashdata('erro', "<div class='alert alert-danger alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i>O usuário já tem uma reserva de sala nesse horário.</h4></div>");
							redirect($this->controller."/reservar/".$idSala);
						}
					}
					else {
						$this->session->set_flashdata('erro', "<div class='alert alert-danger alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i>A sala já tem uma reserva marcada nesse horário</h4></div>");
						redirect($this->controller."/reservar/".$idSala);
					}
				}
				else if ($hoje == $dados['data_inicio']) {
					if ($horario < $dados['hora_inicio']) {
						if(!$this->salas_model->verificarDisponibilidadeSala($idSala, $horarios)){
							if(!$this->salas_model->verificarDisponibilidadeUsuario($this->userId, $horarios)){
								$this->salas_model->realizarReserva($this->userId, $idSala, $horarios);
								$this->session->set_flashdata('cadastro_ok', "1");
								redirect($this->controller."/reservar/".$idSala);
							}
							else {
								$this->session->set_flashdata('erro', "<div class='alert alert-danger alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i>O usuário já tem uma reserva de sala nesse horário.</h4></div>");
								redirect($this->controller."/reservar/".$idSala);
							}
						}
						else {
							$this->session->set_flashdata('erro', "<div class='alert alert-danger alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i>A sala já tem uma reserva marcada nesse horário</h4></div>");
							redirect($this->controller."/reservar/".$idSala);
						}
					}
					else {
						$this->session->set_flashdata('erro', "<div class='alert alert-danger alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i>O horário escolhido já passou.</h4></div>");
						redirect($this->controller."/reservar/".$idSala);
					}
				}
				else {
					$this->session->set_flashdata('erro', "<div class='alert alert-danger alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i>A data escolhida já passou.</h4></div>");
					redirect($this->controller."/reservar/".$idSala);
				}
			}
			else {
				$reservas = $this->salas_model->getHorariosByIdSala($idSala);

				$nome = $this->salas_model->getNomeSala($idSala);
			
				$dados = array(
					"title" => "Horários",
					"pasta" => "salas",
					"tela" => "reservar",
					"action" => base_url().$this->controller.'/reservar/'.$idSala,
					"reservas" => $reservas,
					"username" => $this->username,
					"nomeSala" => $nome[0]
				);
		
				$this->load->view('sistema/layout', $dados);
			}
		}
	}

}