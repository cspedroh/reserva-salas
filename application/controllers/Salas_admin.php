<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salas_admin extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->controller = 'salas_admin';
		$this->load->model(
					array(
						'salas_model'
					)
				);

		if (!$this->ion_auth->is_admin()) {
			redirect('home');
		}
		$this->user = $this->ion_auth->user()->row();
	}

	public function index(){

		$salas = $this->salas_model->getSalas();

		$dados = array(
			"title" => "Salas",
			"pasta" => "salas",
			"tela" => "retrieve",
			"salas" => $salas
		);

		$this->load->view('admin/layout_admin', $dados);
	}

	public function create()
	{
		if (!$this->input->post()) {
			
			$dados = array(
					"title" => "Cadastro de Salas",
					"pasta" => "salas",
					"tela" => "create",
					"action" => base_url().$this->controller."/create"
				);

			$this->load->view('admin/layout_admin', $dados);
		}
		else{

			$dados = $this->input->post();

			$this->salas_model->set($dados);

			$this->session->set_flashdata('cadastro_ok', "1");

			redirect($this->controller);
		}
	}

	public function update($idSala = '')
	{
		if ($idSala != '') {
			
			if (!$this->input->post()) {

				$sala = $this->salas_model->getSalaById($idSala);
			
				$dados = array(
						"title" => "Editar Sala",
						"pasta" => "salas",
						"tela" => "create",
						"action" => base_url().$this->controller."/update/".$idSala,
						"sala" => $sala
					);

				$this->load->view('admin/layout_admin', $dados);
			}
			else{

				$dados = $this->input->post();

				$this->salas_model->update($idSala, $dados);

				$this->session->set_flashdata('cadastro_ok', "1");

				redirect($this->controller);
			}
		}
	}

	public function delete($idSala = '')
	{
		if ($idSala != '') {
			
			if($this->salas_model->delete($idSala)) {

				$this->session->set_flashdata('cadastro_ok', "1");
				redirect($this->controller);
			}
		}
		$this->session->set_flashdata('cadastro_ok', "0");
		redirect($this->controller);
	}

}