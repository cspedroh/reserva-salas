<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservas extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->controller = 'reservas';
		$this->load->model(
					array(
						'reservas_model'
					)
				);

		if (!$this->ion_auth->logged_in()) {
			redirect('home');
		}

		if ($this->ion_auth->is_admin()) {
			redirect('home');
		}
		else{
			$this->user = $this->ion_auth->user()->row();
			$this->username = $this->user->first_name;
			$this->userId = $this->user->id;
		}

		date_default_timezone_set('America/Sao_Paulo');
	}

	public function index()
	{
		$reservas = $this->reservas_model->getReservasByUserId($this->userId);

		$agora = date('Y-m-d H:i:s');

		$dados = array(
			"title" => "Minhas reservas",
			"pasta" => "reservas",
			"tela" => "reservas",
			"reservas" => $reservas,
			"username" => $this->username,
			"agora" => $agora
		);

		$this->load->view('sistema/layout', $dados);
	}

	public function delete($idReserva = '')
	{
		if ($idReserva != '') {
			
			$this->reservas_model->delete($idReserva, $this->userId);
		}
	}

}