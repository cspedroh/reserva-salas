<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->controller = 'home';
	}

	public function index()
	{
		$dados = array(
				"title" => "Área do Administrador",
				"action" => base_url()."home/login"
			);

		$this->load->view('sistema/login/index', $dados);
	}

	// log the user in
	public function login()
	{
		if ($this->input->post('email') != "" && $this->input->post('senha') != "")
		{
			if ($this->ion_auth->login($this->input->post('email'), $this->input->post('senha')))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());

				if (!$this->ion_auth->is_admin())
				{
					$this->user = $this->ion_auth->user()->row();

					redirect('salas');
				}
			}
		}
		$this->session->set_flashdata('message', "<span class='text-red'>E-mail ou senha inválido, tente novamente!</span>");
		redirect('home', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
	}

	public function logout()
	{
		$this->ion_auth->logout();

		redirect('home');
	}
}
