<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Reservas_model extends CI_Model {

	public function getReservasByUserId($idUser = '')
	{
		if ($idUser != '') {
			
			$q = $this->db->select('reservas.*, salas.nome')
			->join('salas', 'reservas.id_sala=salas.id')
			->where('id_user', $idUser)
			->order_by('inicio_reserva desc')
			->get('reservas');

			return $q->result();
		}
	}

	public function delete($idReserva = '', $idUser = '')
	{
		if ($idReserva != '' && $idUser != '') {
			
			$this->db->where('id', $idReserva)
			->where('id_user', $idUser)
			->delete('reservas');

			$this->session->set_flashdata('cadastro_ok', "1");
			redirect($this->controller);
		}
	}

}