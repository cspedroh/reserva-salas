<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	public function deleteReservas($idUser = '')
	{
		if ($idUser != '') {
			
			$this->db->where('id_user', $idUser)
			->delete('reservas');
		}
	}

}