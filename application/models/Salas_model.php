<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Salas_model extends CI_Model {

	public function getSalas()
	{
		$q = $this->db->select('*')
		->get('salas');

		return $q->result();
	}

	public function set($dados = '')
	{
		if ($dados != '') {

			$this->db->insert('salas', $dados);
		}
	}

	public function getSalaById($idSala = '')
	{
		if ($idSala != '') {
			
			$q = $this->db->select('*')
			->where('id', $idSala)
			->get('salas');

			return $q->result();
		}
	}

	public function update($idSala = '', $dados = '')
	{
		if ($idSala != '' && $dados != '') {
			
			$this->db->where('id', $idSala)
			->update('salas', $dados);
		}
	}

	public function delete($idSala = '')
	{
		if ($idSala != '') {
			
			$this->db->where('id', $idSala)
			->delete('salas');

			$this->where('id_sala', $idSala)
			->delete('reservas');

			return true;
		}
	}

	public function getHorariosByIdSala($idSala = '')
	{
		if ($idSala != '') {
			
			$hoje = date('Y-m-d');

			$q = $this->db->select('*')
			->where('id_sala', $idSala)
			->where('inicio_reserva>=', $hoje)
			->get('reservas');

			return $q->result();
		}
	}

	public function verificarDisponibilidadeSala($idSala = '', $dados = '')
	{
		if ($idSala != '' && $dados != '') {
			pre($dados);
			pre($idSala);
			$q = $this->db->select('id')
			->group_start()
				->where('inicio_reserva', $dados['inicio_reserva'])
				->or_group_start()
					->where('inicio_reserva>', $dados['inicio_reserva'])
					->where('inicio_reserva<', $dados['fim_reserva'])
				->group_end()
				->or_group_start()
					->where('fim_reserva>', $dados['inicio_reserva'])
					->where('fim_reserva', $dados['fim_reserva'])
				->group_end()
				->or_group_start()
					->where('inicio_reserva<', $dados['inicio_reserva'])
					->where('fim_reserva>', $dados['inicio_reserva'])
				->group_end()
				->or_group_start()
					->where('inicio_reserva<', $dados['fim_reserva'])
					->where('fim_reserva>', $dados['fim_reserva'])
				->group_end()
			->group_end()
			->where('id_sala', $idSala)
			->get('reservas');

			$resultado = $q->result();
			
			if (count($resultado) > 0) {
				return true;
			}
			else {
				return false;
			}
		}
	}

	public function verificarDisponibilidadeUsuario($idUser = '', $dados = '')
	{
		if ($idUser != '' && $dados != '') {
			
			$q = $this->db->select('id')
			->group_start()
				->where('inicio_reserva', $dados['inicio_reserva'])
				->or_group_start()
					->where('inicio_reserva>', $dados['inicio_reserva'])
					->where('inicio_reserva<', $dados['fim_reserva'])
				->group_end()
				->or_group_start()
					->where('fim_reserva>', $dados['inicio_reserva'])
					->where('fim_reserva', $dados['fim_reserva'])
				->group_end()
				->or_group_start()
					->where('inicio_reserva<', $dados['inicio_reserva'])
					->where('fim_reserva>', $dados['inicio_reserva'])
				->group_end()
				->or_group_start()
					->where('inicio_reserva<', $dados['fim_reserva'])
					->where('fim_reserva>', $dados['fim_reserva'])
				->group_end()
			->group_end()
			->where('id_user', $idUser)
			->get('reservas');

			$resultado = $q->result();

			if (count($resultado) > 0) {
				return true;
			}
			else {
				return false;
			}
		}
	}

	public function realizarReserva($idUser = '', $idSala = '', $dados = '')
	{
		$reserva = array(
			'id_user' => $idUser,
			'id_sala' => $idSala,
			'inicio_reserva' => $dados['inicio_reserva'],
			'fim_reserva' => $dados['fim_reserva']
		);

		$this->db->insert('reservas', $reserva);
	}

	public function getNomeSala($idSala = '')
	{
		if ($idSala != '') {
			
			$q = $this->db->select('nome')
			->where('id', $idSala)
			->get('salas');

			return $q->result();
		}
	}

}