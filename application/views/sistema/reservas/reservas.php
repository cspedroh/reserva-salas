<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo $title?>
			<small>controle de <?php echo $title?> aqui</small>
		</h1>
		<?php
			if ($this->session->flashdata('cadastro_ok') == '1') { ?>
				<div class="alert alert-success alert-dismissible" style="margin-top: 10px; margin-bottom: 0px;">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-check"></i>Reserva cancelada com sucesso!</h4>
				</div>
			<?php }
			elseif ($this->session->flashdata('cadastro_ok') == '0') { ?>
				<div class="alert alert-danger alert-dismissible" style="margin-top: 10px; margin-bottom: 0px;">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-check"></i>Não foi possível realizar a operação!</h4>
				</div>
			<?php }
		?>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Lista de <?php echo $title?></h3>
					</div>
					<div class="box-body">

						<?php
							$tmpl = array('table_open' => '<table class="table table-striped table-inter1">');
							$this->table->set_template($tmpl);

							$this->table->set_heading('Id', 'Nome da sala', 'Horário da reserva', 'Opções');

							foreach ($reservas as $reserva) {

								$delete = "";

								if ($reserva->inicio_reserva <= $agora) {
									$delete = "<span class='label label-default'>Já utilizada</span>";
								}
								else {
									$delete = "<a href='javascript:void(0)' onclick='confirmarExclusao(\"reservas/delete/".$reserva->id."\", \"".$reserva->nome."\")'><button type='button' class='btn btn-danger btn-sm'>Cancelar reserva</button></a>";
								}
								
								$this->table->add_row(
									$reserva->id,
									$reserva->nome,
									date('d/m/Y H:i:s', strtotime($reserva->inicio_reserva))." até ".date('d/m/Y H:i:s', strtotime($reserva->fim_reserva)),
									$delete
								);
							}

							echo $this->table->generate();
						?>
						<div class="box-footer">

						</div>
						<!-- /.box-footer-->
					</div>
					<!-- /.box -->
				</div>
			</div>
		</div>

		<!-- DataTables -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/data-tables/DataTables-1.10.16/css/dataTables.bootstrap.min.css">
		<script src="<?php echo base_url() ?>assets/plugins/data-tables/datatables.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/data-tables/DataTables-1.10.16/js/dataTables.bootstrap.min.js"></script>

		<script type="text/javascript">

			function confirmarExclusao(caminho, nome){
				if (confirm ("Tem certeza que deseja excluir esta reserva na sala -  " + nome + "?")){
					window.location.href = path + caminho;
				}
			}

			$(document).ready(function() {
				$('.table').DataTable({
					"pagingType": "full_numbers",
					"language": {
						"url": "<?php echo base_url() ?>assets/plugins/data-tables/pt-br.json"
					},
					"order": false
				});
			});
		</script>

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->