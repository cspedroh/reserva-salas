<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/fullcalendar-3.9.0/fullcalendar.min.css">
<script src="<?php echo base_url()?>assets/plugins/fullcalendar-3.9.0/lib/moment.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/fullcalendar-3.9.0/fullcalendar.min.js"></script>
<script src='<?php echo base_url()?>assets/plugins/fullcalendar-3.9.0/locale/pt-br.js'></script>

<script type="text/javascript">
	$(function(){
		$('#calendar').fullCalendar({
			defaultView: 'agendaWeek',
			locale: 'pt-br',
			events: [
				<?php
					$count = count($reservas);
					$i = 1;

					foreach ($reservas as $reserva) {
						echo "{ title: 'Reservado', ";
						echo "start: '".$reserva->inicio_reserva."', ";
						echo "end: '".$reserva->fim_reserva."'";
						if($i < $count){
							echo "},";
						}
						else{
							echo "}";
						}
						$i++;
					}
				?>
			]
		});
	});
</script>

<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $title;?></h1>
		<?php echo $this->session->flashdata('erro')?>
		<?php
			if ($this->session->flashdata('cadastro_ok') == '1') { ?>
				<div class="alert alert-success alert-dismissible" style="margin-top: 10px; margin-bottom: 0px;">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-check"></i>Reserva realizada com sucesso!</h4>
				</div>
			<?php }
		?>
	</section>

	<section class="content">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
				<div class="box-tools pull-right"> 
			</div>
		</div>
		
		<form role="form" id="salvar" method="post" action="<?php echo $action ?>">  
			<div class="box-body">
				<div class="col-md-6">
					<div class="col-md-6">
						<div class="form-group">
							<label>Data da reserva</label>
							<input type="date" name="data_inicio" required class="form-control" id="data_inicio">
						</div>
					</div>
					<div class="form-group col-md-6">
						<label>Horário da reserva</label>
						<input type="time" name="hora_inicio" required class="form-control" id="hora_inicio">
					</div>
					<label>Atenção: todas as reservas tem duração de uma hora!</label>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-primary salvar">Salvar</button>
			</div>
		</form>
	</section>
	<!-- /.content -->
	<section class="content">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Agenda de reservas da sala<?php echo (isset($nomeSala) && $nomeSala != '') ? " - ".$nomeSala->nome : "" ?></h3>
				<div class="box-tools pull-right"> 
			</div>
		</div>
		<div class="box-body">
			<div id="calendar"></div>
		</div>
		<div class="box-footer">
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
