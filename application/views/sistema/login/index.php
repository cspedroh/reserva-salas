<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="viewport" content="width=device-width, user-scalable=no">

		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<meta property="og:url"           content="<?php echo site_url()?>" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="Reserva da Salas" />
		<meta property="og:description"   content="Sistema para reserva de salas" />
		<meta property="og:image"         content="<?php echo base_url() ?>images/logo_color.png" />

		<title><?php echo (isset($title)) ? $title  : 'Sistema base'; ?></title>

		<!-- <meta name="DESCRIPTION" content="<?php echo (isset($configHeader->description)) ? $configHeader->description : ''; ?>"/>

		<meta name="KEYWORDS" content="<?php echo (isset($configHeader->keywords)) ? $configHeader->keywords : ''; ?>"/>

		<meta name="OWNER" content="<?php echo (isset($configHeader->email_contato)) ? $configHeader->email_contato : ''; ?>"/>
		<meta name="distribution" content="Global"/>
		<meta name="SUBJECT" content="<?php echo (isset($configHeader->subject)) ? $configHeader->subject : ''; ?>"/> -->
		<meta name="RATING" content="General"/>
		<meta name="ROBOTS" content="INDEX, FOLLOW"/>
		<meta name="Googlebot" content="index,follow"/>
		<meta name="MSNBot" content="index,follow,all"/>
		<meta name="InktomiSlurp" content="index,follow,all"/>
		<meta name="Unknownrobot" content="index,follow,all"/>
		<meta name="REVISIT-AFTER" content="2 days"/>
		<meta name="Audience" content="all"/>
		<!-- <meta name="Abstract" content="<?php echo (isset($configHeader->abstract)) ? $configHeader->abstract : ''; ?>"/> -->

		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/Ionicons/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/admin-lte/css/AdminLTE.css">

		<script>
			var base_url = '<?php echo base_url(); ?>';
			var site_url = '<?php echo site_url(); ?>';
			var path     = base_url;
		</script>

	</head>

	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="<?php echo site_url(); ?>"><b>Controle de Salas</b></a>
			</div>
			
			<div class="login-box-body">
				<p class="login-box-msg">Inicie sua sessão</p>

				<form action="<?php echo $action ?>" method="post">

					<?php echo "<p>".$this->session->flashdata('message')."</p>"; ?>

					<div class="form-group has-feedback">
						<input type="email" required="" class="form-control" placeholder="Email" name="email">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" required="" class="form-control" placeholder="Senha" name="senha">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row" style="margin-left: 7px">		
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
						</div>
						
					</div>
				</form>
			</div>
		</div>
	</body>

</html>
