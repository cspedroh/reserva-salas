<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta property="og:url"           content="<?php echo site_url()?>" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Reserva de Salas" />
	<meta property="og:description"   content="Sistema para reserva de salas" />
	<meta property="og:image"         content="<?php echo base_url() ?>images/logo_color.png" />
	<title><?php echo isset($title) ? $title : ""; ?></title>

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin-lte/css/AdminLTE.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin-lte/css/skins/_all-skins.min.css">
	<!-- css -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">

	<!-- jQuery 3 -->
	<script src="<?php echo base_url()?>assets/jquery-3.2.1/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url()?>assets/admin-lte/js/adminlte.min.js"></script>

	<script>
		var base_url = '<?php echo base_url(); ?>';
		var site_url = '<?php echo site_url(); ?>';
		var path     = base_url;
	</script> 
	
</head>

<!-- ADD THE CLASS sidebar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
<body class="hold-transition skin-blue sidebar-mini fixed">
<!-- Site wrapper -->
<div class="wrapper">

	<header class="main-header">
		<!-- Logo -->
		<a href="<?php base_url().'usuarios' ?>" class="logo">
			<span class="logo-mini"><b>C</b>S</span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>Controle</b> de Salas</span>
			<!-- <img class="logo-lg logo-sorrisus" src="<?php echo base_url()."images/sorrisus-logo.png" ?>"> -->
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>

			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?php echo base_url()."images/generic-user-purple.png" ?>" class="user-image" alt="User Image">
							<span class="hidden-xs"><?php echo (isset($username)) ? $username : "Usuário" ?></span>
						</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header">
								<img src="<?php echo base_url()."images/generic-user-purple.png" ?>" class="img-circle" alt="User Image">
								<p>ADMIN</p>
							</li>
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-right">
									<a href="<?php echo base_url().'home/logout' ?>" class="btn btn-default btn-flat">Sair</a>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- =============================================== -->