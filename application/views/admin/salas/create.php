<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $title;?></h1>
	</section>

	<section class="content">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
				<div class="box-tools pull-right"> 
			</div>
		</div>

		<div style="background: #f39c12;">
			<?php echo $this->session->flashdata('erro');?>
		</div>

		<form role="form" id="salvar" method="post" action="<?php echo $action ?>">  
			<div class="box-body">
				<div class="col-md-6">
					<div class="form-group">
						<label>Nome</label>
						<input type="text" name="nome" required="" class="form-control" id="sala" value="<?php echo isset($sala[0]->nome) ? $sala[0]->nome : '' ?>" placeholder="Nome">
					</div>
				</div>
			</div>

			<div class="box-footer">
				<button type="submit" class="btn btn-primary salvar">Salvar</button>
			</div>

		</form>

	<!-- /.box -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
