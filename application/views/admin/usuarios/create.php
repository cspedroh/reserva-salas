<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $title;?></h1>
	</section>

	<section class="content">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
				<div class="box-tools pull-right"> 
				</div>
			</div>
			
			<div style="background: #f39c12;">
				<?php echo $this->session->flashdata('erro');?>
			</div>

			<form role="form" id="salvar" method="post" action="<?php echo $action ?>">
				<div class="box-body">
					<div class="col-md-6">
						<div class="form-group">
							<label>Nome</label>
							<input type="text" name="nome" required="" class="form-control" id="nome" value="<?php echo (isset($user->first_name)) ? $user->first_name : '' ?>" placeholder="Nome">
						</div>
						<div class="form-group">
							<label>E-mail</label>
							<input type="text" name="email" class="cel form-control" id="email" value="<?php echo (isset($user->email)) ? $user->email : '' ?>" <?php echo (isset($user->email)) ? "disabled" : '' ?><?php echo (!isset($user->email)) ? "required=''" : "" ?> placeholder="E-mail">
						</div>
						<div class="form-group">
							<label>Senha </label><br><?php echo (isset($user->username)) ? "<span style='font-size: 10px'> (Digite um senha caso queria alterar a atual)</span>" : '' ?>
							<input type="password" name="senha" class="form-control" id="password" value="" <?php echo (!isset($user->username)) ? "required=''" : "" ?> placeholder="Senha">
						</div>
					</div>

				<?php
					echo (isset($user) ? "<input type='hidden' name='id' id='id' value=".$user->id.">" : "");
				?>
				</div>

				<div class="box-footer">
					<button type="submit" class="btn btn-primary salvar">Salvar</button>
				</div>

			</form>
				
		</div>
			<!-- /.box -->

	</section>
		<!-- /.content -->
</div>
	<!-- /.content-wrapper -->
