<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo base_url()."images/generic-user-purple.png" ?>" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>ADMIN</p>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">Menu</li>
			<li><a href="<?php echo base_url()."usuarios_admin" ?>"><i class="fa fa-circle-o"></i><span> Usuários</span></a></li>
			<li><a href="<?php echo base_url()."salas_admin" ?>"><i class="fa fa-circle-o"></i><span> Salas</span></a></li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>