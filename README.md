# Reserva de Salas

Sistema para reserva de salas.

## Requisitos do servidor

Versão PHP: 7.2.5

## Banco de dados

Nome do banco: reserva-salas  
host: localhost  
Usuário padrão: root  
Sem senha  
Caso precise configurar host, usuário e/ou senha, editar arquivo /application/config/database.php  
'hostname' => 'localhost'  
'username' => 'root'  
'password' => ''  

## Sistema

Cadastre os usuários e salas na área do admin.

### Área do admin
 * Para acessar: http://localhost/reserva-salas/admin
 * Email : `admin@admin.com` / Password : `password`

### Área do usuário
 * Para acessar: http://localhost/reserva-salas/